###cloud vars

## Переменная для token
variable "token" {
  type        = string
  description = "OAuth-token; https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token"
}

## Переменная для cloud_id
variable "cloud_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/cloud/get-id"
}

## Переменная для default_zone
variable "folder_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id"
}

## Переменная для default_zone
variable "subnet_zone" {
  type        = list(string)
  default     = ["ru-central1-a","ru-central1-b","ru-central1-c"]
  description = "https://cloud.yandex.ru/docs/overview/concepts/geo-scope"
}

variable "cidr" {
  type        = list(string)
  default     = ["10.10.1.0/24","10.10.2.0/24","10.10.3.0/24"]
  description = "https://cloud.yandex.ru/docs/overview/concepts/geo-scope"
}

## Переменная для vpc_name
variable "vpc_name" {
  type        = string
  default     = "develop"
  description = "VPC network&subnet name"
}

## Переменная для public_key
variable "public_key" {
  type    = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCWfdyRrURnDkQTKNib5vpWZiPRHZGLOrYcrJvzAATIw8GsvKMv9WyWHslOtRNLayA+5DZOD9wjvf0QV7pPrOHQnzDKfMMxzFj8ZnCkaGUMKB5jK9SfHoAaLoZijpVV/nT0lHsuvcbuy4DFnlXpUe1MN9F4MHbc8g/84IoQk2NBeOU3geehMN1E1GKTJfenK+9Ulnb9zTh80KU6GyKYCvvdL9gjuBXMLJwBPpf/y4Oeh8OKRh4OySSZN2qoMzrRLDSOvHtHOZZa67yluWBO0cpz/IVp8IPEW5jqq3wwxYw6oUj4RpGXYtKuJCkkVqnUsLCATIwI18Gl3j/5NdEi7WxaqHJuQDj2OZfkDHeVT8QMUk5bB3fcBLB3BLomSJwnvbyWkuAtqGxU+HurCDk8MWCkG6YbeQhCAdYPmiyUO4nWVNfvtXjl4+rcDgW+V+/9FflVuTXn5PtLRkQuErDh246bmpqfQvilEODScgxWtqPEMo7WHTWPr+t4DE/kvFAi+r8= philipp@philipp-VirtualBox"
}

variable "ubuntu-2004-lts" {
  default = "fd8m2ak64lipvicd94sf"
}