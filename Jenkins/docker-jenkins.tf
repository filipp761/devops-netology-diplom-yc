resource "yandex_compute_instance" "platform1" {
  count = 1
  name        = "docker-jenkins"
  zone = "ru-central1-a"
  platform_id = "standard-v3"
  hostname = "docker-jenkins"
  allow_stopping_for_update = true


  resources {
    cores         = 4
    memory        = 4
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = var.ubuntu-2004-lts
      type = "network-hdd"
      size = 25
    }
  }

## Прерываемая
  scheduling_policy {
    preemptible = true
  }


  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet_zones[count.index].id}"
    nat       = true
  }

  metadata = {
    serial-port-enable = 1
    ssh-keys           = "ubuntu:${var.public_key}"
  }

}