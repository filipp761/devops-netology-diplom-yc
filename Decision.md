# Дипломный практикум в Yandex.Cloud
  * [Цели:](#цели)
  * [Этапы выполнения:](#этапы-выполнения)
     * [Создание облачной инфраструктуры](#создание-облачной-инфраструктуры)
     * [Создание Kubernetes кластера](#создание-kubernetes-кластера)
     * [Создание тестового приложения](#создание-тестового-приложения)
     * [Подготовка cистемы мониторинга](#подготовка-cистемы-мониторинга)
     * [Установка и настройка CI/CD](#установка-и-настройка-cicd)

---
## Цели:

1. Подготовить облачную инфраструктуру на базе облачного провайдера Яндекс.Облако.
2. Запустить и сконфигурировать Kubernetes кластер.
3. Установить и настроить систему мониторинга.
4. Настроить и автоматизировать сборку тестового приложения с использованием Docker-контейнеров.
5. Настроить CI для автоматической сборки.
6. Настроить CD для автоматического развёртывания приложения.

---


## Этапы выполнения:

### Создание облачной инфраструктуры

1. Установил версию Terraform 1.5.7

    ![terraform_version](./img/terraform_version.png)

2. Создал сервисный аккаунт, который будет в дальнейшем использоваться Terraform для работы с инфраструктурой с необходимыми и достаточными правами.

3. Создал бакет для хранения состояния инфраструктуры создаваемой Terraform. Написал манифест для создания bucket и сервисного аккаунта [бакет](./terraform/backet.tf)

    ![terraform_version](./img/YC_bucket.png)

4. Создал VPC с подсетями в разных зонах доступности. [VPC](./terraform/network.tf)
    ![terraform_version](./img/YC_subnet.png)
5. Убедился, что теперь могу выполнить команды terraform destroy и terraform apply без дополнительных ручных действий (Повторный прогон команды terraform apply)
    ![terraform_version](./img/terraform_apply.png)

Получил ожидаемые результаты:
1. Terraform сконфигурирован и создание инфраструктуры посредством Terraform возможно без дополнительных ручных действий.
    ![terraform_version](./img/YC_all.png)
    ![terraform_version](./img/YC_node.png)
---
### Создание Kubernetes кластера

Создать кластер я решил вручную
  1. Написал ansible-playbook для установки необходимого ПО на ноды:

      - [ansible-playbook](./ansible/k8s-cluster.yaml)

  2. Установка мастера ```kubeadm init``` - Инициализируем кластер

Эта команда подготовит и установит все необходимое для работы вашего Kubernetes кластера, а также протестирует его работоспособность

В конце выполнения kubeadm выдаст вам команду и токен для подключения ваших вычислительных узлов.

  3. Даем доступ для обычного пользователя
  ```sh
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
  ```

  4. Настройка виртуальной сети
  ```sh
  curl -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.1/manifests/calico.yaml -O
  ```
  ```sh
  kubectl apply -f calico.yaml
  ```
  5. Подлючаем ноды к кластеру (команду для подлючения будет показана в конце установки мастера)
  ```sh
  kubeadm join 10.10.1.5:6443 --token h23x3a.xci0hnu70szo4kex \
        --discovery-token-ca-cert-hash sha256:9cb1a60df793bc60fdc743b04442a152a63e5d729d43a291808f61ca9d7cc513
  ```
  6. Проверяем, что ноды подсоединились
  ![k8s](./img/kubectl_get_nodes.png)
  ![k8s](./img/kubectl_get_all.png)
---
### Создание тестового приложения

1. Создал [репозиторий](https://gitlab.com/filipp761/devops-diplom-app) с тестовым приложением

2. Сделал простенький [Dockerfile](https://gitlab.com/filipp761/devops-diplom-app/-/blob/main/Dockerfile)

    ```Dockerfile
    FROM nginx:1.23.3

    # Configuration 
    ADD conf /etc/nginx
    # Content
    ADD content /usr/share/nginx/html

    EXPOSE 80 
    ```
3. А так же [манифест](https://gitlab.com/filipp761/devops-diplom-app/-/blob/main/manifest.yaml) k8s для деплоя

    ```yaml
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      labels:
        app: app-web
      name: app-web
      namespace: stage
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: app-web
      template:
        metadata:
          labels:
            app: app-web
        spec:
          containers:
            - image: 130958750/nginxapp:gitTag
              imagePullPolicy: IfNotPresent
              name: app-web
          terminationGracePeriodSeconds: 30
          
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: app-web
      namespace: stage
    spec:
      ports:
        - name: web
          port: 80
          targetPort: 80
          nodePort: 30090
      selector:
        app: app-web
      type: NodePort


4. Создал неймспейс diploma-app в кластере

    ```bash
    kubectl create ns stage
    ```
___
### Подготовка cистемы мониторинга

1. Я воспользовался пакетом [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus) и скопировал манифесты себе в папку [monitoring](./monitoring/)

2. Применяем манифесты

    ```bash
    kubectl apply --server-side -f manifests/setup
    kubectl wait --for condition=Established --all CustomResourceDefinition --namespace=monitoring
    kubectl apply -f manifests/
    kubectl apply -f grafana-service.yaml
    ```
3. Чтобы иметь доступ к Grafana снаружи кластера, пришлось удалить сетевые политики, которые запрещают доступ через балансировщик нагрузки.

```sh
kubectl -n monitoring delete networkpolicies.networking.k8s.io --all
```
4. Получаем доступ к Grafana через NodePort

```sh
kubectl --namespace monitoring patch svc grafana -p '{"spec": {"type": "NodePort"}}'
```
5. Открываем веб интерфейс и проверяем что дашборды работают
![Grafana](./img/Grafana.png)
![Grafana](./img/Grafana_1.png)

6. Http доступ к тестовому приложению.
```sh
ubuntu@node-0:~$ kubectl get deploy -n stage
NAME      READY   UP-TO-DATE   AVAILABLE   AGE
app-web   1/1     1            1           3d23h
ubuntu@node-0:~$ kubectl get svc -n stage
NAME      TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
app-web   NodePort   10.107.126.88   <none>        80:30080/TCP   3d23h
```
![Nginx_Web_Container](./img/Nginx_Web_Container.png)

### Установка и настройка CI/CD

Для выполнения этого задания использовал CI/CD [jenkins](https://www.jenkins.io/)

В YC была развернута виртуальная машина с дальнейшей установкой на ней ci/cd с помощью docker container.
![YC_docker_jenkins](./img/YC_docker_jenkins.png)

Cоздан собственный образ. Dockerfile для создания

![jenkins-blueocean_dh](./img/jenkins-blueocean_dh.png)

Далее подключаюсь на созданную машину и выполняю скрипт jenkins-install.sh Поднимаются контейнеры jenkins и dind(нужен для работы docker).

![docker_ps_jenkins](./img/docker_ps_jenkins.png)

1. Интерфейс ci/cd сервиса доступен по http.

![Jenkins](./img/Jenkins.png)

2. При любом коммите в репозиторие с тестовым приложением происходит сборка и отправка в регистр Docker образа.

Автоматический запуск pipline выполняется с помощью настройки webhooks в репозитории приложения. По определению наличия изменений.

![webhooks](./img/Github_webhooks.png)

  ```sh
ubuntu@node-0:~/devops-diplom-app$ git status
On branch main
Your branch is ahead of 'origin/main' by 4 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   content/index.html

no changes added to commit (use "git add" and/or "git commit -a")
ubuntu@node-0:~/devops-diplom-app$ git add .
ubuntu@node-0:~/devops-diplom-app$ git status
On branch main
Your branch is ahead of 'origin/main' by 4 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   content/index.html

ubuntu@node-0:~/devops-diplom-app$ git commit -m "commit_all tag=1.7"
[main 58d6654] commit_all tag=1.7
 1 file changed, 1 insertion(+), 1 deletion(-)
ubuntu@node-0:~/devops-diplom-app$ git tag -a 1.7 -m "my version app 1.7"
ubuntu@node-0:~/devops-diplom-app$ git push origin 1.7
Username for 'https://github.com': filipp761
Password for 'https://filipp761@github.com':
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 484 bytes | 484.00 KiB/s, done.
Total 5 (delta 2), reused 0 (delta 0)
remote: Resolving deltas: 100% (2/2), completed with 2 local objects.
To https://github.com/filipp761/devops-diplom-app.git
 * [new tag]         1.7 -> 1.7
ubuntu@node-0:~/devops-diplom-app$
  ```
![pipeline1](./img/pipeline1.png)

![pipeline1_docker](./img/pipeline1_docker.png)

3. При создании тега (например, v1.0.0) происходит сборка и отправка с соответствующим label в регистр, а также деплой соответствующего Docker образа в кластер Kubernetes.

Создаем pipline который будет выполнять последний пункт нашего задания.

* [Jenkinsfile](./Jenkins/Pipeline_Build-tagImage-deploy)

Меняем версию контент-файла в гите и отправляем в наш репозиторий.

```sh
ubuntu@node-0:~$ git push origin --tags
```
![Pipeline Build-tagImage-deploy](./img/Pipeline_Build-tagImage-deploy.png)

<details>
<summary>Вывод лога pipeline</summary>

```commandline

Started by GitHub push by filipp761
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/Pipeline Build-tagImage-deploy
[Pipeline] {
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Checkout Source)
[Pipeline] git
The recommended git tool is: NONE
No credentials specified
 > git rev-parse --resolve-git-dir /var/jenkins_home/workspace/Pipeline Build-tagImage-deploy/.git # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://github.com/filipp761/devops-diplom-app # timeout=10
Fetching upstream changes from https://github.com/filipp761/devops-diplom-app
 > git --version # timeout=10
 > git --version # 'git version 2.30.2'
 > git fetch --tags --force --progress -- https://github.com/filipp761/devops-diplom-app +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision f171aac57205134f70c0c7c971daeb6618c13e1d (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f f171aac57205134f70c0c7c971daeb6618c13e1d # timeout=10
 > git branch -a -v --no-abbrev # timeout=10
 > git branch -D main # timeout=10
 > git checkout -b main f171aac57205134f70c0c7c971daeb6618c13e1d # timeout=10
Commit message: "Update manifest.yaml"
 > git rev-list --no-walk 4f74d77a40e88214608dfdc7f685eb4254ac7db2 # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Checkout tag)
[Pipeline] script
[Pipeline] {
[Pipeline] sh
+ git fetch
From https://github.com/filipp761/devops-diplom-app
 * branch            HEAD       -> FETCH_HEAD
[Pipeline] sh
+ head -n 1
+ git tag --sort=-creatordate
[Pipeline] echo
gitTag output: 1.10
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Build image)
[Pipeline] script
[Pipeline] {
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker build -t 130958750/nginxapp .
#0 building with "default" instance using docker driver

#1 [internal] load build definition from Dockerfile
#1 transferring dockerfile: 148B done
#1 DONE 0.1s

#2 [internal] load metadata for docker.io/library/nginx:1.23.3
#2 DONE 0.2s

#3 [internal] load .dockerignore
#3 transferring context: 2B done
#3 DONE 0.0s

#4 [1/3] FROM docker.io/library/nginx:1.23.3@sha256:f4e3b6489888647ce1834b601c6c06b9f8c03dee6e097e13ed3e28c01ea3ac8c
#4 DONE 0.0s

#5 [internal] load build context
#5 transferring context: 474B done
#5 DONE 0.0s

#6 [2/3] ADD conf /etc/nginx
#6 CACHED

#7 [3/3] ADD content /usr/share/nginx/html
#7 CACHED

#8 exporting to image
#8 exporting layers done
#8 writing image sha256:fff2166c3136b517dd8a523046fd49fec8fbf2003d05d70b46435e07fff52635 done
#8 naming to docker.io/130958750/nginxapp done
#8 DONE 0.0s
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Pushing Image:tags)
[Pipeline] withEnv
[Pipeline] {
[Pipeline] script
[Pipeline] {
[Pipeline] withEnv
[Pipeline] {
[Pipeline] withDockerRegistry
$ docker login -u 130958750 -p ******** https://index.docker.io/v1/
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /var/jenkins_home/workspace/Pipeline Build-tagImage-deploy@tmp/64cb19af-e7f6-4f17-b3f8-c10bc6bf51c0/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
[Pipeline] {
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker tag 130958750/nginxapp index.docker.io/130958750/nginxapp:1.10
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker push index.docker.io/130958750/nginxapp:1.10
The push refers to repository [docker.io/130958750/nginxapp]
8f209f10a57a: Preparing
a9788233370b: Preparing
a1bd4a5c5a79: Preparing
597a12cbab02: Preparing
8820623d95b7: Preparing
338a545766ba: Preparing
e65242c66bbe: Preparing
3af14c9a24c9: Preparing
3af14c9a24c9: Waiting
8820623d95b7: Waiting
338a545766ba: Waiting
e65242c66bbe: Waiting
3af14c9a24c9: Layer already exists
8f209f10a57a: Layer already exists
8820623d95b7: Layer already exists
a9788233370b: Layer already exists
597a12cbab02: Layer already exists
a1bd4a5c5a79: Layer already exists
e65242c66bbe: Layer already exists
338a545766ba: Layer already exists
1.10: digest: sha256:16cceb48ec3d4e1f544d90d5cdaffde98eeb83e057570dc021fda3013fc2a7de size: 1984
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // withDockerRegistry
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (sed env)
[Pipeline] withEnv
[Pipeline] {
[Pipeline] script
[Pipeline] {
[Pipeline] sh
+ sed -i 18,22 s/gitTag/1.10/g manifest.yaml
[Pipeline] sh
+ cat manifest.yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: app-web
  name: app-web
  namespace: stage
spec:
  replicas: 1
  selector:
    matchLabels:
      app: app-web
  template:
    metadata:
      labels:
        app: app-web
    spec:
      containers:
        - image: 130958750/nginxapp:1.10
          imagePullPolicy: IfNotPresent
          name: app-web
      terminationGracePeriodSeconds: 30
          
---
apiVersion: v1
kind: Service
metadata:
  name: app-web
  namespace: stage
spec:
  ports:
    - name: web
      port: 80
      targetPort: 80
      nodePort: 30090
  selector:
    app: app-web
  type: NodePort
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Deploying manifest to Kubernetes)
[Pipeline] sshagent
[ssh-agent] Using credentials ubuntu (k8s-credentials)
[ssh-agent] Looking for ssh-agent implementation...
[ssh-agent]   Exec ssh-agent (binary ssh-agent on a remote machine)
$ ssh-agent
SSH_AUTH_SOCK=/tmp/ssh-SN5YSimAyHRp/agent.6223
SSH_AGENT_PID=6226
Running ssh-add (command line suppressed)
Identity added: /var/jenkins_home/workspace/Pipeline Build-tagImage-deploy@tmp/private_key_894571522522333215.key (ubuntu@docker-jenkins)
[ssh-agent] Started.
[Pipeline] {
[Pipeline] sh
+ scp -o StrictHostKeyChecking=no manifest.yaml ubuntu@178.154.204.35:/home/ubuntu/devops-diplom-app
[Pipeline] script
[Pipeline] {
[Pipeline] sh
+ ssh ubuntu@178.154.204.35 kubectl apply -f ./devops-diplom-app/manifest.yaml
deployment.apps/app-web configured
service/app-web unchanged
[Pipeline] }
[Pipeline] // script
[Pipeline] }
$ ssh-agent -k
unset SSH_AUTH_SOCK;
unset SSH_AGENT_PID;
echo Agent pid 6226 killed;
[ssh-agent] Stopped.
[Pipeline] // sshagent
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```
</details>

* log web-hooks

![GitHub Log](./img/GitHub_Log.png)

* image созданный в процессе

![Pipeline_Build-tagImage-deploy_docker](./img/Pipeline_Build-tagImage-deploy_docker.png)

* pod на нашем кластере kubernetes

![Pipeline_Build-tagImage-deploy_pod](./img/Pipeline_Build-tagImage-deploy_pod.png)

* web доступ к запущенному приложению с отметкой номера тега(для наглядности).

![Pipeline_Build-tagImage-deploy_web](./img/Pipeline_Build-tagImage-deploy_web.png)

Выполняем сборку еще раз теперь уже используя Blue Ocean

![BlueOcean](./img/BlueOcean.png)

![Pipeline_Build-tagImage-deploy_docker](./img/Pipeline_Build-tagImage-deploy_docker.png)

![Pipeline_Build-tagImage-deploy_web_BO](./img/Pipeline_Build-tagImage-deploy_web_BO.png)

